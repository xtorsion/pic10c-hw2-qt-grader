# PIC10C HW2 QT Grader

This project was meant to practice playing with QTCreator.

The project itself is a GUI that calculates the grades for different classes, one fictional, and one real (PIC10C). Everything was designed with QTDesigner.
Schema A for the fictional class is 25% HW, 20% Midterm1, 20% Midterm2, 35% Final. Schema B is 25% HW, 30% Highest Midterm, 45% Final.
Schema A for PIC10C is 15% HW, 25% Midterm, 30% Final, 35% Final Project. Schema B is 15% HW, 50% Final, 35% Final Project.
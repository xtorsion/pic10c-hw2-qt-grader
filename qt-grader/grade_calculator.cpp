#include "grade_calculator.h"
#include "ui_grade_calculator.h"
#include <algorithm>
#include <vector>

grade_calculator::grade_calculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::grade_calculator)
{
    ui->setupUi(this);

    //connects all the sliders with respective spinBoxes
    QObject::connect(ui->spinBox,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_2,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_2,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_3,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_3,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_4,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_4,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_5,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_5,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_6,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_6,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_7,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_7,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_8,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_8,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_9,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_9,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_10,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_10,SLOT(setValue(int)));
    QObject::connect(ui->spinBox_11,SIGNAL(valueChanged(int)),
                     ui->horizontalSlider_11,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider,SIGNAL(valueChanged(int)),
                     ui->spinBox,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_2,SIGNAL(valueChanged(int)),
                     ui->spinBox_2,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_3,SIGNAL(valueChanged(int)),
                     ui->spinBox_3,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_4,SIGNAL(valueChanged(int)),
                     ui->spinBox_4,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_5,SIGNAL(valueChanged(int)),
                     ui->spinBox_5,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_6,SIGNAL(valueChanged(int)),
                     ui->spinBox_6,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_7,SIGNAL(valueChanged(int)),
                     ui->spinBox_7,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_8,SIGNAL(valueChanged(int)),
                     ui->spinBox_8,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_9,SIGNAL(valueChanged(int)),
                     ui->spinBox_9,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_10,SIGNAL(valueChanged(int)),
                     ui->spinBox_10,SLOT(setValue(int)));
    QObject::connect(ui->horizontalSlider_11,SIGNAL(valueChanged(int)),
                     ui->spinBox_11,SLOT(setValue(int)));
}

grade_calculator::~grade_calculator()
{
    delete ui;
}

//Calculates the class score on pushing the "Calculate!" button
void grade_calculator::on_pushButton_clicked()
{
    //This is for the class defined in the spec
    if (ui->classChoice->currentIndex() == 0)
    {
        //This calculates grade for schema A
        if (ui->radioButton->isChecked())
        {
            int hw_min = std::min({ui->spinBox->value(), ui->spinBox_2->value(), ui->spinBox_3->value(), ui->spinBox_4->value(),
                                ui->spinBox_5->value(), ui->spinBox_6->value(), ui->spinBox_7->value(), ui->spinBox_8->value()});
            int hw_total = ui->spinBox->value() + ui->spinBox_2->value() + ui->spinBox_3->value() + ui->spinBox_4->value()
                    + ui->spinBox_5->value() + ui->spinBox_6->value() + ui->spinBox_7->value() + ui->spinBox_8->value() - hw_min;
            double hw_average = static_cast<double>(hw_total) / 7.0;
            double midterm1 = static_cast<double> (ui->spinBox_9->value());
            double midterm2 = static_cast<double> (ui->spinBox_10->value());
            double final = static_cast<double> (ui->spinBox_11->value());
            double score = 0.25 * hw_average + 0.2 * midterm1 + 0.2 * midterm2 + 0.35 * final;

            ui->overallScore->setText(QString::number(score));

            return;
        }
        //This calculates grade for schema B
        else if (ui->radioButton_2->isChecked())
        {
            int hw_min = std::min({ui->spinBox->value(), ui->spinBox_2->value(), ui->spinBox_3->value(), ui->spinBox_4->value(),
                                ui->spinBox_5->value(), ui->spinBox_6->value(), ui->spinBox_7->value(), ui->spinBox_8->value()});
            int hw_total = ui->spinBox->value() + ui->spinBox_2->value() + ui->spinBox_3->value() + ui->spinBox_4->value()
                    + ui->spinBox_5->value() + ui->spinBox_6->value() + ui->spinBox_7->value() + ui->spinBox_8->value() - hw_min;
            double hw_average = static_cast<double>(hw_total) / 7.0;
            double midterm_max = static_cast<double> (std::max(ui->spinBox_9->value(), ui->spinBox_10->value()));
            double final = static_cast<double> (ui->spinBox_11->value());
            double score = 0.25 * hw_average + 0.3 * midterm_max + 0.45 * final;

            ui->overallScore->setText(QString::number(score));

            return;
        }
    }

    //This calculates grades for PIC10C
    else if (ui->classChoice->currentIndex() == 1)
    {
        //This calculates grade for schema A
        if (ui->radioButton->isChecked())
        {
            int hw_total = ui->spinBox->value() + ui->spinBox_2->value() + ui->spinBox_3->value();
            double hw_average = static_cast<double>(hw_total) / 3.0;
            double midterm = static_cast<double> (ui->spinBox_9->value());
            double final_project = static_cast<double> (ui->spinBox_10->value());
            double final = static_cast<double> (ui->spinBox_11->value());
            double score = 0.15 * hw_average + 0.25 * midterm + 0.3 * final + 0.35 * final_project;

            ui->overallScore->setText(QString::number(score));

            return;
        }
        //This calculates grade for schema B
        else if (ui->radioButton_2->isChecked())
        {
            int hw_total = ui->spinBox->value() + ui->spinBox_2->value() + ui->spinBox_3->value();
            double hw_average = static_cast<double>(hw_total) / 3.0;
            double final_project = static_cast<double> (ui->spinBox_10->value());
            double final = static_cast<double> (ui->spinBox_11->value());
            double score = 0.15 * hw_average + 0.5 * final + 0.35 * final_project;

            ui->overallScore->setText(QString::number(score));

            return;
        }
    }
}

//This is used to change the labels on the differences between the class requirements
void grade_calculator::on_classChoice_currentIndexChanged(int index)
{
    //These are the labels for PIC10C
    if (index == 1)
    {
        ui->label_4->setText("N/A");
        ui->label_5->setText("N/A");
        ui->label_6->setText("N/A");
        ui->label_7->setText("N/A");
        ui->label_8->setText("N/A");
        ui->label_10->setText("Final Project");
    }

    //These are the labels for the class defined in the spec
    else if (index == 0)
    {
        ui->label_4->setText("HW4");
        ui->label_5->setText("HW5");
        ui->label_6->setText("HW6");
        ui->label_7->setText("HW7");
        ui->label_8->setText("HW8");
        ui->label_10->setText("Midterm 2");
    }

    return;
}
